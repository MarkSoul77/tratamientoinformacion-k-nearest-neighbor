# Implementación propia del algoritmo K-Nearest-Neighbor (k vecinos más cercanos)
En este proyecto se dará seguimiento a mi implementación del algoritmo para machine learning
de K vecinos cercanos, con la teoria recibida del curso de tratamiento de la información prim 2022.
La implementación del algoritmo se lleva a cabo en JavaScript en un entorno aplicación WEB.

# Notas

ASOS PARA CALCULAR LA DESVIACION ESTANDAR
1. calcular el promedio de los valores;
2. a cada valor hay que restarle el promedio, y el resultado elevarlo al cuadrado;
3. sumar todos los valores obtenidos;
4. dividir la suma por la cantidad de valores; y.
5. sacar la raíz cuadrada del resultado.
const arr2 = [5,15,12,18,28];

# Seguimiento de ramas y commit's


Rama master:

commit: `01_initial_commit`

 - commit inicial donde se subieron los avances que llevo.

commit : `02_avances_leerArchivo`

 - se suben avances y ya se puede leer un archivo.
 - falta estructurarlo en arrays para poder emplearlos.
