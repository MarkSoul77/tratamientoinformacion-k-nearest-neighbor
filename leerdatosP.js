const fs = require('fs');

// leemos el archivo:
//const data = fs.readFileSync('./datos.txt', 'utf-8');
const data = fs.readFileSync('./sb1-P.txt', 'utf-8');

// le hace parse al los datos:
function parseCSV(text) {
    // Obtenemos las lineas del texto
    let lines = text.replace(/\r/g, '').split('\n');
    return lines.map(line => {
      // Por cada linea obtenemos los valores
      let values = line.split(',');
      return values;
    });
}


// extraemos los datos conforme a las propiedades:
const arrPropiedades = (numprop=0, arr=[]) => {
    const matrix = [];
    let j=0;
    while(j<numprop+1){
        const aux = [];
        let i = 3;
        while(i<arr.length){
            if(arr[2][j] === '0'){
                aux.push(Number(arr[i][j]));
            }else{
                aux.push(arr[i][j]);
            }
            i++;
        }
        matrix.push(aux);
        j++;
    }
    return matrix;
}

const arrElementos = (numelem=0, numprop=0, arr=[]) => {
    const matrix = [];
    let j=3;
    while(j<numelem+3){
        const aux = [];
        let i = 0;
        while(i<numprop+1){
            if(arr[2][i] === '0'){
                aux.push(Number(arr[j][i]));
            }else{
                aux.push(arr[j][i]);
            }
            i++;
        }
        matrix.push(aux);
        j++;
    }
    return matrix;
}



// obtenemos el resultado del parseo:
const parseo = parseCSV(data);

// obtenemos los datos de la cabecera:
// obtenemos el numero de elementos;
const numElementosP = Number(parseo[0][0]);
// obtenemos el número de propiedades:
const numPropiedadesP = Number(parseo[1][0]);
// obtenemos el tipo de propiedades:
const tipos = parseo[2];
// comvertimos los props en enteros:
const tiposInt = tipos.map( e => Number(e) );


// obtenemos los datos de las propiedades:
const matrizElementosP = arrElementos(numElementosP, numPropiedadesP, parseo);
const matrizPropiedadesP = arrPropiedades(numPropiedadesP, parseo);


//console.log(numElementosP, numPropiedadesP);
//console.log(matrizElementosP);
//console.log(matrizPropiedadesP, '\n------------Cálculos--------------');

// exportamos datos;
module.exports = {
    numElementosP,
    numPropiedadesP,
    matrizElementosP,
    matrizPropiedadesP
}