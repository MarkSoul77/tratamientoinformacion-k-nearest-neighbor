const {numElementos, numPropiedades, matrizElementos, matrizPropiedades} = require('./leerdatosT');
const {numElementosP, numPropiedadesP, matrizElementosP, matrizPropiedadesP} = require('./leerdatosP');

// ----------- FUNCIONES PARA PARÁMETROS NO NUMÉRICOS...
// cuantas veces la propiedad exp1  y exp2  en la clases
const Naxc = function (exp1='', classx='', arrexp=[], arrclass=[]){
    cont = 0;
    for(let i=0; i<arrexp.length; i++){
        if(arrexp[i] === exp1 && arrclass[i] === classx) cont++;
    }
    return cont;
}
// numero de veces que exp1 está en arr[]
const Nax = function (exp1='', arr=[]) {
    const Nax = arr.filter(element => element === exp1);
    return Nax.length
}
// función que retorna la distacia:
const disnonum = (param1='', param2='', arr=[], arrclass=[], numclases=0, clasesunicas=[]) => {
    let suma = 0;
    for(let i=0; i<numclases; i++){
        op1 = Naxc(param1, clasesunicas[i], arr, arrclass);
        op2 = Nax(param1, arr);
        op3 = Naxc(param2, clasesunicas[i], arr, arrclass);
        op4 = Nax(param2, arr);
        suma = suma + ((op1/op2) - (op3/op4));
        //console.log(`${op1}/${op2} - ${op3}/${op4} = ${suma}`);
    }
    return Math.abs(suma);
}


//const p = disnonum('f', 'e', credit, clases, numclases);
//const p2 = disnonum()
//console.log(Math.pow(p,2));




// ----------    FUNCIONES PARA PARÁMETROS NUMÉRICOS.....
const promedio = function (arr=[]){
    let suma = 0;
    let div = arr.length;
    for(let i of arr) suma += i;
    return suma/div;
}
const desviaStandar = function (arr=[]){
    const prom = promedio(arr);
    const cant = arr.length;
    let suma = 0;
    for(let i of arr) suma += (Math.pow(i-prom,2));
    const varianza = suma/(cant-1);
    const desviaS = Math.sqrt(varianza);
    let ds;
    desviaS === 0 ? ds = 0 : ds = desviaS.toFixed(3)
    return ds;
}
const disnum = (param1=0, param2=0, arrnum=[]) => {
    let result = 0;
    const resta = Math.abs(param1-param2); 
    const ds = desviaStandar(arrnum);
    ds === 0 ? result = 0 : result = (resta/(4*ds));
    //console.log(`${resta}/4*${ds} = ${result}`);
    return result;
}
//disnum(13500, 6500, income);



// ---------------------- funciones extras:
const clasesUnitarias = (arr = []) => {
    const nr = [];
    let c = 0;
    // recorremos los elementos del arr:
    for(let i of arr){
        // preguntamos si ese elemento existe en el nuevo arr:
        if(!nr.includes(i)){
            // sino existe lo añadimos:
            nr.push(i);
        }
    }
    return nr;
}
// funcion para obtener un elemento de matriz de elementos (sin la clase);
const obtenerElemento = (index=0) => {
    const arr=[];
    const elemento = matrizElementos[index];
    for(let i=0; i<numPropiedades; i++){
        if(i !== numPropiedades+1){
            arr.push(elemento[i]);
        }
    }
    return arr;
}
const obtenerElementoP = (index=0) => {
    const arr=[];
    const elemento = matrizElementosP[index];
    for(let i=0; i<numPropiedadesP; i++){
        if(i !== numPropiedadesP+1){
            arr.push(elemento[i]);
        }
    }
    return arr;
}
// función para calcular los indices de k vecinos mas cercano:
const kVecinos = (distancias=[], arrdesorden=[], k=0) => {
    const vecinos = [];
    for(let i=0; i<k; i++){
        const index = arrdesorden.indexOf(distancias[i]);
        if(index !== -1){
            vecinos.push(index);
        }
    }
    return vecinos;
}
// funion para calcular o predecir su vecino más cercano:
const vecino = (vecinos=[], clases=[]) => {
    const vecinosclases = [];
    
    for(let e of vecinos){
        vecinosclases.push(clases[e]);
    }
    for( let e of vecinosclases){

    }
}



// obtenemos el array de clases y sus propiedades:
const clases = matrizPropiedades[matrizPropiedades.length-1];
const clasesUnicas = clasesUnitarias(clases);
const numclases = clasesUnicas.length;
const clasesP = matrizPropiedadesP[matrizPropiedadesP.length-1];
const clasesUnicasP = clasesUnitarias(clasesP);
const numclasesP = clasesUnicasP.length;



const hvdm = ( oN=[], k=1 ) => {
    // arrays donde se guadaran las distancias:
    const distancias = [];
    const distanciascopia = [];
    
    // j son el numero de elementos que se van a calcular, para el ejemplo 7:
    let j=0;
    while(j<numElementos){

        let i = 0;
        let suma = 0;
        // obtenemos eN :
        const eN = obtenerElemento(j);
        
        while(i<oN.length){

            let dis = 0;
            // discretización de si es un número
            if(!(typeof(oN[i]) === 'number')){ // no es numero
                const arrprop = matrizPropiedades[i];
                dis = Math.pow(disnonum(eN[i], oN[i], arrprop, clases, numclases, clasesUnicas), 2);

            }else{ // es numero
                const arrprop = matrizPropiedades[i];
                dis = Math.pow(disnum(oN[i], eN[i], arrprop), 2);
            }
            console.log(dis);
            // vamos sumando el cuadrado de las distancias:
            suma = suma + dis;
            i++;
            
        }
        // agregamos la suma a un array de sumas:
        distancias.push(suma);
        
        j++;
    }

    console.log('\n -------Array de distancias---------');
    distancias.map( (element, i) => {
        console.log(`\n Distancias de oN con todos los eN \n En(${i}) = ${element}`);
    });
    distancias.map( e => {
        distanciascopia.push(e);
    });

    // ordenamos las distancias:
    distancias.sort();

    // aplicamos k-vecinos.
    const vecinos = kVecinos(distancias, distanciascopia, k);
    
    console.log('\n -------K vecinos más cercanos y sus clases---------');
    // buscamos los vecinos en las clases:
    for(let e of vecinos){
        console.log(`\nClase: ${clases[e]}`);
    }
    
    return clases[vecinos[0]];
}


const exactitud = () => {
    const result = [];
    let aciertos=0;
    let i=0;
    while(i<numElementosP){
        const elemento = obtenerElementoP(i);
        result.push(hvdm(elemento,3));
        i++;
    }
    console.log('\n -------comparación de arrays (result - clases) ---------');
    console.log(result);
    console.log(clasesP);
    let j=0;
    for( let e of result){
        if(e === clasesP[j]){
            aciertos++;
        }
        j++;
    }
    const exactitud = ((aciertos/numElementosP)*100);
    console.log(`\n -------Exactitud: %${exactitud} ---------`);
}




const oN = ['0',  '1', '2', '1', '0', '3', '1',
'1',  '0', 2,   '1', '1', '0', '2',
'2',  '0', '0', '0', '1', '0', '1',
'2',  '0', 0,   '0', 0,   '0', '3',
'4',  '0', '0', '0', 0,   0,   '0'];
 
//const oN = [6500, '0', '0'];
//const v = hvdm(oN, 3);
//console.log(v);


exactitud();






