const {numElementos, numPropiedades, tiposInt, matrizElementos, matrizPropiedades} = require('./leerdatos');

// ARREGLOS CON LOS QUE SE ESTÁ TRABAJANDO...
const income = [13500, 15600, 18000, 7300, 2750, 2550, 3400]
const studets = ['yes','yes','no','yes','yes','no','no'];   // yes=0, no=1
const credit =  ['f'  , 'e' , 'f', 'f' , 'f' , 'e', 'e'];   // f=0, e=1
const clases =   ['h'  ,'h'  ,'l' ,'l'  ,'l'  ,'h' , 'l'];   // h=0, l=1



// obtenemos las clases unitarias(únicas) en un array y num  clases totales:
const clasesUnitarias = (arr = []) => {
    const nr = [];
    let c = 0;
    // recorremos los elementos del arr:
    for(let i of arr){
        // preguntamos si ese elemento existe en el nuevo arr:
        if(!nr.includes(i)){
            // sino existe lo añadimos:
            nr.push(i);
        }
    }
    return nr;
}
const clasesUnicas = clasesUnitarias(clases);
const numclases = clasesUnicas.length;





// ----------- FUNCIONES PARA PARÁMETROS NO NUMÉRICOS...

// cuantas veces la propiedad exp1  y exp2  en la clases
const Naxc = function (exp1='', exp2='', arr1=[], arr2=[]){
    cont = 0;
    for(let i=0; i<arr1.length; i++){
        if(arr1[i] === exp1 && arr2[i] === exp2) cont++;
    }
    return cont;
}
// numero de veces que exp1 está en arr[]
const Nax = function (exp1='', arr=[]) {
    const Nax = arr.filter(element => element === exp1);
    return Nax.length
}
// función que retorna la distacia:
const disnonum = (param1='', param2='', arr=[], arrclass=[], numclases=0) => {
    let suma = 0;
    for(let i=0; i<numclases; i++){
        op1 = Naxc(param1, clasesUnicas[i], arr, arrclass);
        op2 = Nax(param1, arr);
        op3 = Naxc(param2, clasesUnicas[i], arr, arrclass);
        op4 = Nax(param2, arr);
        suma = suma + ((op1/op2) - (op3/op4));
        console.log(`${op1}/${op2} - ${op3}/${op4} = ${suma}`);
    }
    return Math.abs(suma);
}

//const p = disnonum('yes', 'no', studets, clases, numclases);
//const p = disnonum('f', 'e', credit, clases, numclases);
//const p2 = disnonum()
//console.log(Math.pow(p,2));




// ----------    FUNCIONES PARA PARÁMETROS NUMÉRICOS.....
const promedio = function (arr=[]){
    let suma = 0;
    let div = arr.length;
    for(let i of arr) suma += i;
    return suma/div;
}
const desviaStandar = function (arr=[]){
    const prom = promedio(arr);
    const cant = arr.length;
    let suma = 0;
    for(let i of arr) suma += (Math.pow(i-prom,2));
    const varianza = suma/(cant-1);
    const desviaS = Math.sqrt(varianza);
    const ds = desviaS.toFixed(3);
    return ds;
}
const disnum = (param1=0, param2=0, arrnum=[]) => {
    let result = 0;
    const resta = Math.abs(param1-param2);
    const ds = desviaStandar(arrnum);
    result = (resta/(4*ds));
    console.log(`${resta}/4*${ds} = ${result}`);
    return result;
}
//disnum(13500, 6500, income);




// ------------- FUNCIONES PARA EXTRAS ---------

// funcion que obtiene cada expresión (eN):
const expresion = (index=0) =>{
    const arr = [];
    arr.push(income[index]);
    arr.push(studets[index]);
    arr.push(credit[index]);
    return arr;
}

// función para calcular el k vecino mas cercano:
const kVecinos = (distancias=[], arrdesorden=[], k=0) => {
    const vecinos = [];
    for(let i=0; i<k; i++){
        const index = arrdesorden.indexOf(distancias[i]);
        if(index !== -1){
            vecinos.push(index);
        }
    }
    return vecinos;
}




// ----------  FUNCIONES PRINCIPALES:


const oN = [6500, 'no', 'e'];



const hvdm = ( oN=[] ) => {
    
    const distancias = [];
    const distanciascopia = [];
    // j son el numero de elementos que se van a calcular, para el ejemplo 7:
    let j=0;
    while(j<7){
        let i = 0;
        let suma = 0;
        // obtenemos eN :
        const eN = expresion(j);
        
        while(i<oN.length){

            let dis = 0;
            // discretización de si es un número
            if(isNaN(oN[i])){ // no es numero

                if((oN[i] === 'yes') || (oN[i] === 'no')){
                    dis = Math.pow(disnonum(oN[i], eN[i], studets, clases, numclases), 2);

                }
                if((oN[i] === 'f') || (oN[i] === 'e')){
                    dis = Math.pow(disnonum(oN[i], eN[i], credit, clases, numclases), 2);
                }
                
            }else{ // es numero
                dis = Math.pow(disnum(oN[i], eN[i], income), 2);
            }
            console.log(dis);
            // vamos sumando el cuadrado de las distancias:
            suma = suma + dis;
            i++;
            
        }

        // agregamos la suma a un array de sumas:
        distancias.push(suma);
        
        j++;
        
    }

    // mostramos las distancias obtenidas:
    distancias.map( (element, i) => {
        console.log(`\n Distancias de oN con todos los eN \n En(${i}) = ${element}`);
    } );
    distancias.map( e => {
        distanciascopia.push(e);
    })

    // ordenamos las distancias:
    distancias.sort();

    // aplicamos k-vecinos.
    const vecinos = kVecinos(distancias, distanciascopia, 3);
    
    // buscamos los vecinos en las clases:
    for(let e of vecinos){
        console.log(`\nClase: ${clases[e]}`);
    }
    
}



// PRUEBAS...

hvdm(oN);