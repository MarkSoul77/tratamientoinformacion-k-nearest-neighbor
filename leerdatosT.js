const fs = require('fs');

// leemos el archivo:
//const data = fs.readFileSync('./datos.txt', 'utf-8');
const data = fs.readFileSync('./sb1-T.txt', 'utf-8');

// le hace parse al los datos:
function parseCSV(text) {
    // Obtenemos las lineas del texto
    let lines = text.replace(/\r/g, '').split('\n');
    return lines.map(line => {
      // Por cada linea obtenemos los valores
      let values = line.split(',');
      return values;
    });
}


// extraemos los datos conforme a las propiedades:
const arrPropiedades = (numprop=0, arr=[]) => {
    const matrix = [];
    let j=0;
    while(j<numprop+1){
        const aux = [];
        let i = 3;
        while(i<arr.length){
            if(arr[2][j] === '0'){
                aux.push(Number(arr[i][j]));
            }else{
                aux.push(arr[i][j]);
            }
            i++;
        }
        matrix.push(aux);
        j++;
    }
    return matrix;
}

const arrElementos = (numelem=0, numprop=0, arr=[]) => {
    const matrix = [];
    let j=3;
    while(j<numelem+3){
        const aux = [];
        let i = 0;
        while(i<numprop+1){
            if(arr[2][i] === '0'){
                aux.push(Number(arr[j][i]));
            }else{
                aux.push(arr[j][i]);
            }
            i++;
        }
        matrix.push(aux);
        j++;
    }
    return matrix;
}



// obtenemos el resultado del parseo:
const parseo = parseCSV(data);

// obtenemos los datos de la cabecera:
// obtenemos el numero de elementos;
const numElementos = Number(parseo[0][0]);
// obtenemos el número de propiedades:
const numPropiedades = Number(parseo[1][0]);
// obtenemos el tipo de propiedades:
const tipos = parseo[2];
// comvertimos los props en enteros:
const tiposInt = tipos.map( e => Number(e) );


// obtenemos los datos de las propiedades:
const matrizElementos = arrElementos(numElementos, numPropiedades, parseo);
const matrizPropiedades = arrPropiedades(numPropiedades, parseo);


//console.log(numElementos, numPropiedades);
//console.log(matrizElementos);
//console.log(matrizPropiedades, '\n------------Cálculos--------------');

// exportamos datos;
module.exports = {
    numElementos,
    numPropiedades,
    matrizElementos,
    matrizPropiedades
}


//console.log(matrizPropiedades[matrizPropiedades.length-1]);









































/*


// maneras de convertir string a array:
//const datos = Array.from(data)
const arrayCaracteres = [...data];


const caracteres = returCaracteres(arrayCaracteres);

// retornamos un array con puro caracter y salto de linea:
const returCaracteres = (arr=[]) => {
    const na = [];
    for(let i of arr){
        if((i !== '\r') && (i !== '\n') ){
            na.push(i);
        }
    }
    return na;
}


    while(cont<2){
        if(ban === 0){
            let numcomas=0;
            let i=2;
            while (numcomas<prop) {
                if(arr[i] === ','){
                    numcomas++;
                    i++;
                }else{
                    aux.push(arr[i]);
                    i++;
                }
            }
            aux.push(arr[i]);
            iarray = i+1;
            na.push(aux);
            ban=1;
        }else{
            // limpiamos nuestro array auxiliar:
            //aux.map(() => aux.pop());
            const aux2 = [];
            
            while(iarray<arr.length){
                console.log(arr.length);
                if(arr[iarray] === ','){
                    //console.log('entra el if');
                    na.push(aux2);
                    console.log(na);
                    aux2.map(() => aux.pop());
                    console.log(na);
                    iarray++;
                    
                }else{
                    aux2.push(arr[iarray]);
                    iarray++;
                }
                
                
            }
            
        }
        cont++;
    }


    const probando = (texto, separador='', omitirE=false) => {
    return texto.slice( omitirE ? texto.indexOf('\n') + 1 : 0)
        .split('\n')
        .map(l => l.split(separador));
}

const prueba = (arr=[], nelem=0, prop=0) => {
    
    const na = [];
    
    
    let index = 2;
    let sig = 1
    let j=0;

    while(j<1){
        const aux = [];
        let numcomas=0;
        let i=index;
        while (sig !== 0) {
            if(numcomas<prop){
                if(arr[i] === ','){
                    numcomas++;
                    i++;
                }else{
                    aux.push(arr[i]);
                    i++;
                }
            }else{
                sig = 0;
            }
        }
        //aux.push(arr[i]);
        index = i+1;
        na.push(aux);

        j++;
    }

    

    return na;
}

*/