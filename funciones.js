

// sujeto de prueba
const daniel = [37,50,2];
// sujetos de entrenamiento
const jonh      = [35,35,3];
const rachael   = [22,50,2];
const ruth      = [63,200,1];
const norah     = [25,40,4];


function rCuadrado(x, y){
    return Math.pow((x-y),2);
}

// esta funcion va a calcular la distancia euclidiana que existe entre sujetos u objetos:
function distancia(s1 = [], s2 = []){
    let sum = 0;
    for(let i=0;i<3; i++) sum += rCuadrado(s1[i],s2[i]);
    return Math.sqrt(sum);
}


console.log(distancia(jonh, daniel));
console.log(distancia(rachael, daniel));
console.log(distancia(ruth, daniel));
console.log(distancia(norah, daniel));

const arr = [];
arr[0] = distancia(jonh, daniel);
arr[1] = distancia(rachael, daniel);
arr[2] = distancia(ruth, daniel);
arr[3] = distancia(norah, daniel);

// método de ordenamiento por Selection Sort
const selectionSort = arr => {
    for ( let j = 0; j < arr.length; ++j ) {
      let i = iMin = j;
      for ( ++i; i < arr.length; ++i ) {
        ( arr[ i ] < arr[ iMin ] ) && ( iMin = i );
      }
      [ arr[ j ], arr[ iMin ] ] = [ arr[ iMin ], arr[ j ] ];
    }
    return arr;
}

// ya solo faltaría mostrar sólo los k vecinos más cercanos
// eh identificar la clase...

console.log(selectionSort(arr));
